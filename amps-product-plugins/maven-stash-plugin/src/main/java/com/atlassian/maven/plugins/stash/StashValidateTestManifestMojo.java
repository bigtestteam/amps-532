package com.atlassian.maven.plugins.stash;

import com.atlassian.maven.plugins.amps.osgi.ValidateTestManifestMojo;

import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name = "validate-test-manifest")
public class StashValidateTestManifestMojo extends ValidateTestManifestMojo
{
}
